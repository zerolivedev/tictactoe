
describe("TicTacToe", () => {
  it("starts without fields taken", () => {

    const game = new TicTacToe()

    const noFieldsTaken = []
    expect(game.fieldsTaken()).toBe(noFieldsTaken)
  })

  it("takes fields for a player", () => {
    const game = new TicTacToe()

    const field = { x: 1, y: 1 }
    game.playerTake(field)

    const playerField = { x: 1, y: 1, player: 'X'}
    expect(game.fieldsTaken()).toBe([playerField])
  })

  it("takes fields for another player", () => {
    const game = new TicTacToe()
    game.playerTake({ x: 1, y: 1 })

    game.playerTake({ x: 1, y: 2 })

    expect(game.fieldsTaken().length).toBe(2)
    expect(game.fieldsTaken()[0]).toBe({ x: 1, y: 1, player: 'X'})
    expect(game.fieldsTaken()[1]).toBe({ x: 1, y: 2, player: 'O'})
  })

  it("is playing while there is no winner or draw", () => {
    const game = new TicTacToe()

    const status = game.status()

    expect(status).toBe("Playing")
  })

  it("is playing while there is no winner or draw", () => {
    const game = new TicTacToe()
    game.playerTake({ x: 1, y: 1 })

    const status = game.status()

    expect(status).toBe("Playing")
  })

  it("is won by a player", () => {
    const game = new TicTacToe()
    playUntilFirstPlayerWins(game)

    const status = game.status()

    expect(status).toBe("Player X wins")
  })

  it("is won by another player", () => {
    const game = new TicTacToe()
    playUntilSecondPlayerWins(game)

    const status = game.status()

    expect(status).toBe("Player O wins")
  })

  it("is draw when all the fields are taken and there are no winner", () => {
    const game = new TicTacToe()
    playUntilDraw(game)

    const status = game.status()

    expect(status).toBe("Draw")
  })

  function playUntilDraw(game) {
    game.playerTake({ x: 1, y: 2 })
    game.playerTake({ x: 1, y: 1 })
    game.playerTake({ x: 1, y: 3 })
    game.playerTake({ x: 2, y: 2 })
    game.playerTake({ x: 2, y: 1 })
    game.playerTake({ x: 2, y: 3 })
    game.playerTake({ x: 3, y: 2 })
    game.playerTake({ x: 3, y: 1 })
    game.playerTake({ x: 3, y: 3 })
  }

  function playUntilFirstPlayerWins(game) {
    game.playerTake({ x: 1, y: 1 })
    game.playerTake({ x: 2, y: 1 })
    game.playerTake({ x: 1, y: 2 })
    game.playerTake({ x: 2, y: 2 })
    game.playerTake({ x: 1, y: 3 })
  }

  function playUntilSecondPlayerWins(game) {
    game.playerTake({ x: 2, y: 1 })
    game.playerTake({ x: 1, y: 1 })
    game.playerTake({ x: 2, y: 2 })
    game.playerTake({ x: 1, y: 2 })
    game.playerTake({ x: 3, y: 2 })
    game.playerTake({ x: 1, y: 3 })
  }
})
