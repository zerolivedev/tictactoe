
describe("State", () => {
  it("is playing when there are no taken fields", () => {
    const fields = new Fields()

    const state = State.calculateWith(fields)

    expect(state).toBe("Playing")
  })

  it("is draw when all fields are taken and there are no winner", () => {
    const fields = new Fields()
    takeUntilDraw(fields)

    const state = State.calculateWith(fields)

    expect(state).toBe("Draw")
  })

  it("is won when a player takes a row", () => {
    const fields = new Fields()
    fields.take({ x: 1, y: 1, player: "X" })
    fields.take({ x: 1, y: 2, player: "X" })
    fields.take({ x: 1, y: 3, player: "X" })
    fields.take({ x: 2, y: 1, player: "O" })
    fields.take({ x: 2, y: 2, player: "O" })

    const state = State.calculateWith(fields)

    expect(state).toBe("Player X wins")
  })

  it("is won when a player takes another row", () => {
    const fields = new Fields()
    fields.take({ x: 1, y: 1, player: "O" })
    fields.take({ x: 1, y: 2, player: "O" })
    fields.take({ x: 2, y: 1, player: "X" })
    fields.take({ x: 2, y: 2, player: "X" })
    fields.take({ x: 2, y: 3, player: "X" })

    const state = State.calculateWith(fields)

    expect(state).toBe("Player X wins")
  })

  it("is won when another player takes a row", () => {
    const fields = new Fields()
    fields.take({ x: 1, y: 1, player: "O" })
    fields.take({ x: 1, y: 2, player: "O" })
    fields.take({ x: 1, y: 3, player: "O" })
    fields.take({ x: 2, y: 1, player: "X" })
    fields.take({ x: 2, y: 2, player: "X" })

    const state = State.calculateWith(fields)

    expect(state).toBe("Player O wins")
  })

  it("is playing while there is no winner or is draw", () => {
    const fields = new Fields()
    fields.take({ x: 1, y: 1, player: "X" })
    fields.take({ x: 1, y: 2, player: "X" })
    fields.take({ x: 2, y: 3, player: "X" })
    fields.take({ x: 2, y: 1, player: "O" })
    fields.take({ x: 2, y: 2, player: "O" })

    const state = State.calculateWith(fields)

    expect(state).toBe("Playing")
  })

  it("is won when a player takes a column", () => {
    const fields = new Fields()
    fields.take({ x: 1, y: 1, player: "X" })
    fields.take({ x: 2, y: 1, player: "X" })
    fields.take({ x: 3, y: 1, player: "X" })
    fields.take({ x: 2, y: 2, player: "O" })
    fields.take({ x: 2, y: 3, player: "O" })

    const state = State.calculateWith(fields)

    expect(state).toBe("Player X wins")
  })

  it("is won when another player takes a column", () => {
    const fields = new Fields()
    fields.take({ x: 1, y: 1, player: "O" })
    fields.take({ x: 2, y: 1, player: "O" })
    fields.take({ x: 3, y: 1, player: "O" })
    fields.take({ x: 2, y: 2, player: "X" })
    fields.take({ x: 2, y: 3, player: "X" })

    const state = State.calculateWith(fields)

    expect(state).toBe("Player O wins")
  })

  it("is won when a player takes a diagonal", () => {
    const fields = new Fields()
    fields.take({ x: 1, y: 1, player: "X" })
    fields.take({ x: 2, y: 2, player: "X" })
    fields.take({ x: 3, y: 3, player: "X" })

    const state = State.calculateWith(fields)

    expect(state).toBe("Player X wins")
  })

  it("is won when a player takes another diagonal", () => {
    const fields = new Fields()
    fields.take({ x: 1, y: 3, player: "X" })
    fields.take({ x: 2, y: 2, player: "X" })
    fields.take({ x: 3, y: 1, player: "X" })

    const state = State.calculateWith(fields)

    expect(state).toBe("Player X wins")
  })

  it("is won when another player takes a diagonal", () => {
    const fields = new Fields()
    fields.take({ x: 1, y: 1, player: "O" })
    fields.take({ x: 2, y: 2, player: "O" })
    fields.take({ x: 3, y: 3, player: "O" })

    const state = State.calculateWith(fields)

    expect(state).toBe("Player O wins")
  })

  function takeUntilDraw(fields) {
    fields.take({ x: 1, y: 2, player: "X" })
    fields.take({ x: 1, y: 1, player: "O" })
    fields.take({ x: 1, y: 3, player: "X" })
    fields.take({ x: 2, y: 2, player: "O" })
    fields.take({ x: 2, y: 1, player: "X" })
    fields.take({ x: 2, y: 3, player: "O" })
    fields.take({ x: 3, y: 2, player: "X" })
    fields.take({ x: 3, y: 1, player: "O" })
    fields.take({ x: 3, y: 3, player: "X" })
  }
})
