
const Fields = class {
  constructor() {
    this.fields = []
  }

  take(field) {
    this.fields.push(field)
  }

  toList() {
    return this.fields
  }

  quantity() {
    return this.fields.length
  }

  retrieveOwnedBy(name) {
    return this.fields.filter(field => field.player === name)
  }

  areOdd() {
    return (this.quantity() % 2 !== 0)
  }

  areAllTaken() {
    return (this.quantity() === 9)
  }
}
