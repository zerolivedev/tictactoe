
const State = class {
  static CONDITIONS_FOR_WIN = [
    [{ x: 1, y: 1 }, { x: 2, y: 2 }, { x: 3, y: 3 }],
    [{ x: 1, y: 3 }, { x: 2, y: 2 }, { x: 3, y: 1 }],
    [{ x: 1, y: 1 }, { x: 1, y: 2 }, { x: 1, y: 3 }],
    [{ x: 2, y: 1 }, { x: 2, y: 2 }, { x: 2, y: 3 }],
    [{ x: 3, y: 1 }, { x: 3, y: 2 }, { x: 3, y: 3 }],
    [{ x: 1, y: 1 }, { x: 2, y: 1 }, { x: 3, y: 1 }],
    [{ x: 1, y: 2 }, { x: 2, y: 2 }, { x: 3, y: 2 }],
    [{ x: 1, y: 3 }, { x: 2, y: 3 }, { x: 3, y: 3 }],
  ]

  static calculateWith(fields) {
    if (this._isPlayerXWinner(fields)) {
      return "Player X wins"
    }
    if (this._isPlayerOWinner(fields)) {
      return "Player O wins"
    }

    if (fields.areAllTaken()) {
      return "Draw"
    } else {
      return "Playing"
    }
  }

  static _isPlayerOWinner(fields) {
    return this._meetsAnyConditionToWinPlayer('O', fields)
  }

  static _isPlayerXWinner(fields) {
    return this._meetsAnyConditionToWinPlayer('X', fields)
  }

  static _meetsAnyConditionToWinPlayer(name, fields) {
    const playerFields = fields.retrieveOwnedBy(name)

    return this.CONDITIONS_FOR_WIN.some((fieldsToWin) => {
      return this._includesAll(fieldsToWin, playerFields)
    })
  }

  static _includesAll(fields, anotherFields) {
    return fields.every((field) => {
      return this._includes(field, anotherFields)
    })
  }

  static _includes(field, playerFields) {
    return playerFields.some((playerField) => {
      return (
        field.x === playerField.x &&
        field.y === playerField.y
      )
    })
  }
}
