
const TicTacToe = class {
  FIRST_PLAYER_NAME = 'X'
  SECOND_PLAYER_NAME = 'O'

  constructor() {
    this.fields = new Fields()
  }

  playerTake(field) {
    const playerField = this._playerObtain(field)

    this.fields.take(playerField)
  }

  fieldsTaken() {
    return this.fields.toList()
  }

  status() {
    return State.calculateWith(this.fields)
  }

  _playerObtain(field) {
    return {
      x: field.x,
      y: field.y,
      player: this._currentPlayer()
    }
  }

  _currentPlayer() {
    let currentPlayer = this.FIRST_PLAYER_NAME

    if (this._isSecondPlayerTurn()) {
      currentPlayer = this.SECOND_PLAYER_NAME
    }

    return currentPlayer
  }

  _isSecondPlayerTurn() {
    return this.fields.areOdd()
  }
}
